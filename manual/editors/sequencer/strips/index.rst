
##############
  Strip Types
##############

.. toctree::
   :maxdepth: 2

   introduction.rst
   scene.rst
   mask.rst
   image_movie.rst
   effects/index.rst
   audio.rst
   meta.rst
