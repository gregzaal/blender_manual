
*************
Gaussian Blur
*************

The Guassian Blur strip is used to blur the input strip in the defined direction.
This can be used to blur a background or to blur though a transition (see image). 

.. figure:: /images/editors_sequencer_strips_blur_example.png

   Example of Bluring a Transition.


Options
=======

Size X
   Distance of the blur effect on the X axis.
Size Y
   Distance of the blur effect on the X axis.
