.. _troubleshooting-index:

##################
  Troubleshooting
##################

.. toctree::
   :maxdepth: 1

   Startup <startup.rst>
   3D Viewport <3d_view.rst>
   Graphics Hardware <gpu.rst>
   Crashes <crash.rst>
   Python Errors <python.rst>
   Recovering Lost Work <recover.rst>


Compatibility
=============

Some applications which integrate themselves into your system can cause problem's with Blender.

Here is a list of
`known compatibility issues <http://wiki.blender.org/index.php/Dev:Source/Development/Compatibility>`__.

